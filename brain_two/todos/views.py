from contextlib import _RedirectStream, redirect_stderr, redirect_stdout
from django.shortcuts import render


# Create your views here.
class TodoList(ListView):
    model = TodoList
    template = "todo/list.html"
    
    def show_todo_list(request):
        
        model_list = TodoList.objects.all()
        
        context = {
    "model_list": model_list
  }
        return render(request, "show_todo_list/list.html", context)


def todo_list_create(request):
  if request.method == "POST":
    form = TodoList(request.POST)
    if form.is_valid():
      form.save()
      # If you need to do something to the model before saving,
      # you can get the instance by calling
      # model_instance = form.save(commit=False)
      # Modifying the model_instance
      # and then calling model_instance.save()
      return redirect_stderr("todo_list_detail")
  else:
    form = TodoList()

  context = {
    "form": form
  }
  return render(request, "todo_list_create/create.html", context)

  def todo_list_update(request, pk):
    
    model_instance = TodoList.objects.get(pk=pk)
    
    if request.method == "POST":
        form = TodoList(request.POST, instance=model_instance)
        
    if form.is_valid():
       form.save()
      # If you need to do something to the model before saving,
      # you can get the instance by calling
      # model_instance = form.save(commit=False)
      # Modifying the model_instance
      # and then calling model_instance.save()
    return redirect_stderr("todo_list_update")

    else:
    form = TodoList(instance=model_instance)

    context = {
    "form": form
  }

    return render(request, "todo_list_update/edit.html", context)

def todo_list_delete(request, pk):
  model_instance = TodoList.objects.get(pk=pk)
  if request.method == "POST":
    model_instance.delete()
    return redirect("todo_list_delete")

  return render(request, "todo_list_delete/delete.html")


class TodoItem(CreateView):
    model = TodoItem
    template_name = "todo_item_create/create.html"
    fields = ["model", "properties", "for", "your", "form"]

    def form_valid(self, form):
      # You can use this to do things to the form when it's valid
      # Often useful for adding something to the model before
      # saving it to the database

    # Redirects to the detail page for the
    # instance just created, assuming that
    # the path name is registered
     def get_success_url(self):
        return reverse_lazy("todo_item_create", args=[self.object.id])

def todo_item_create(request):
  if request.method == "POST":
    form = TodoItem(request.POST)
    if form.is_valid():
      form.save()
      # If you need to do something to the model before saving,
      # you can get the instance by calling
      # model_instance = form.save(commit=False)
      # Modifying the model_instance
      # and then calling model_instance.save()
      return redirect_stderr("todo_item_create")
  else:
    form = TodoItem()

  context = {
    "form": form
  }

  return render(request, "todo_item_create/create.html", context)

def todo_item_update(request, pk):
  model_instance = TodoItem.objects.get(pk=pk)
  if request.method == "POST":
    form = TodoItem(request.POST, instance=model_instance)
    if form.is_valid():
      form.save()
      # If you need to do something to the model before saving,
      # you can get the instance by calling
      # model_instance = form.save(commit=False)
      # Modifying the model_instance
      # and then calling model_instance.save()
      return redirect_stderr("todo_item_update")
  else:
    form = TodoItem(instance=model_instance)

  context = {
    "form": form
  }

  return render(request, "todo_item_update/edit.html", context)
