from cgitb import html
from django.db import models
from todos.models import TodoList

TodoList = TodoList.objects.create(name="Reminders")
# Create your models here.
class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.Model.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.name 
    
class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(null=True, blank = True)
    is_completed = models.BooleanField(defalt=False)
    list = models.ForeignKey("TodoList", related_name="items")
    
    def __str__(self):
        return self.task
    